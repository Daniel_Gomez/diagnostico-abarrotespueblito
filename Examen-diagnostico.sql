create database abarrotespueblito;
use abarrotespueblito;
create table Proveedor(IDProveedor int primary key auto_increment,nombre varchar(100),direccion varchar(100),telefono varchar(100));
create table Productos(IDProducto int primary key auto_increment,nombreP varchar(100),Precio double ,fkIDCategoria int, foreign key (fkIDCategoria)references Categorias(IDCategoria));
create table Categorias(IDCategoria int primary key auto_increment,nombre varchar(100));
create table Fechas(IDFecha int primary key auto_increment,FechaEntrega varchar(100),cantidad int,fkIDProveedor int, fkIDProducto int,
foreign key (fkIDProveedor) references Proveedor(IDProveedor),foreign key (fkIDProducto) references Productos(IDProducto));
insert into Proveedor values(null,'Paco','Las vegas','7534598075'),(null,'pancho','La Loma','8340275849');
insert into Categorias values(null,'Refrescos'),(null,'sabritas');
insert into Productos values(null,'Caballito',10,1),(null,'Doritos',15,2);
insert into Fechas values(null,'10/03/2020',20,1,1),(null,'26/03/2020',10,2,2);

create view V_Prove as 
select nombre,nombreP,cantidad,precio from Proveedor,Productos,Fechas where IDProveedor = fkIDProveedor and IDProducto = fkIDProducto;

select * from  V_Prove;/*ignorar*/
/*consulta*/
select nombreP,nombre from Productos,Categorias where IDCategoria = fkIDCategoria order by nombre asc;

# region procedimiento
create procedure insertarProveedor(in _fkIDProveedor int, in _fkIDProducto int, in _cantidad int, in _FechaEntrega varchar(100))
begin
declare x int;
if _cantidad >=1 then
insert into Fechas values(null,_fkIDProveedor, _fkIDProducto ,_cantidad ,_FechaEntrega);
else
select "cantidad no aceptada";
end if;
end;

call insertarProveedor(1,1,null,'2020/03/12');
select * from Fechas;
#end region 